import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Herois, HeroisPage} from "../herois/lista-de-herois/herois";

@Injectable({
  providedIn: 'root'
})

export class HeroisService {
  private readonly API= 'http://superheroapi.com/api/4447536792023053/'

  constructor(private http: HttpClient) {

  }

  list() {
    return this.http.get<HeroisPage>(this.API + "search/a");
  }

  getById(id) {
    return this.http.get<Herois>(this.API + id);
  }

  buscarNomeDigitado(nome) {
    return this.http.get<HeroisPage>(this.API + "search/" + nome);
  }
}

