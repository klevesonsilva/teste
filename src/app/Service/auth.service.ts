import {EventEmitter, Injectable} from '@angular/core';
import {Login } from "../herois/login/Login";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  exibirMenuEmitter = new EventEmitter<boolean>();

  constructor(private router: Router) {
  }

  logar(login: Login) {
    if (login.usuario === 'admin' &&
      login.senha === '123456') {
      localStorage.setItem("usuarioLogado", "true");
      this.exibirMenuEmitter.emit(true);
      this.router.navigate(['/']);

    } else {
      this.exibirMenuEmitter.emit(false);
    }
  }
  usuarioEstaAutenticado(){
    return localStorage.getItem("usuarioLogado")
  }
}
