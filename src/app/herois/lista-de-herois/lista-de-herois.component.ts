import {Component, OnInit} from '@angular/core';
import {HeroisService} from "../../Service/HeroisService";
import {Herois, HeroisPage} from "./herois";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-lista-de-herois',
  templateUrl: './lista-de-herois.component.html',
  styleUrls: ['./lista-de-herois.component.scss']
})
export class ListaDeHeroisComponent implements OnInit {

  herois: Herois[] = [];
   form: FormGroup;


  constructor(private service: HeroisService, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.service.list().subscribe(response => this.herois = response.results);
    this.buscarForm();
  }

  buscarForm(){
    this.form = this.formBuilder.group({
      nome: [null]
    })

}
  buscarNomeDigitado() {
    this.service.buscarNomeDigitado(this.form.value.nome).subscribe(response => this.herois = response.results)

  }

}



