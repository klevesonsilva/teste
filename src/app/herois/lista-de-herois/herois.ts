export interface HeroisPage{
  response: string;
  results: Herois[];



}

export interface Herois {
  id: number;
  name: string;
  powerstats: PowerStats;
  biography: any;
  appearance: Appearance;
  work: Work;
  connections: Connections;
  image: Image;
}

export interface PowerStats {
  intelligence: number;
  strength: number;
  speed: number;
  durabilit: number;
  power: number;
  comba: number;
}

export interface Biography {
  fullName: string;
  alterEgos: string;
  placeofBirth: string;
  firstAppearance: string;
  publisher: string;
  alignment: string;

}

export interface Appearance{

  gender: string;
  race: string;
  height: number;
  weight: number;
  eyeColor: string;
  hairColor: string;

}
export  interface Work {
  occupation:string;
  base: string;

}
export  interface Connections{
  groupAffiliation: string;
  relatives: string;

}

export  interface Image {
  url: string;
}
