import { Component, OnInit } from '@angular/core';
import {HeroisService} from "../../Service/HeroisService";
import {Herois} from "../lista-de-herois/herois";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-detalhes-heroi',
  templateUrl: './detalhes-heroi.component.html',
  styleUrls: ['./detalhes-heroi.component.scss']
})
export class DetalhesHeroiComponent implements OnInit {
  herois: Herois;
  id: Herois;


  constructor(private service:HeroisService,
              private route: ActivatedRoute) {

  }

  ngOnInit(): void {


    this.id = this.route.snapshot.params["id"]

    this.service.getById(this.id).subscribe(response => this.herois = response);
  }

}
