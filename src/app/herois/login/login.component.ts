import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../Service/auth.service";
import {Login} from "./Login";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SwallUtil} from "../util/SwallUtil";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login: Login
  form: FormGroup

  constructor(private authService: AuthService, private formBuilder: FormBuilder) {



  }

  ngOnInit(): void {
    this.formlogin();

  }


  logar() {
    if(this.form.invalid){
      SwallUtil.mensagemError("E obrigatorio preenchimento dos campos, Login e Senha.")
      return;
    }
    this.authService.logar(this.form.value);

  }

  formlogin() {
    this.form = this.formBuilder.group({
      usuario: ['',Validators.required],
      senha: ['',Validators.required]

    })
  }
}
