import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListaDeHeroisComponent} from "./lista-de-herois/lista-de-herois.component";
import {DetalhesHeroiComponent} from "./detalhes-heroi/detalhes-heroi.component";
import {LoginComponent} from "./login/login.component";
import {AuthGuard} from "../Service/auth.guard";

const routes: Routes = [
  {path:"", component:ListaDeHeroisComponent, canActivate:[AuthGuard]},
  {path:"heroi/:id", component:DetalhesHeroiComponent, canActivate:[AuthGuard]},
  {path: "login", component:LoginComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeroisRoutingModule { }
