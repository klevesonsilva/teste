import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeroisRoutingModule } from './herois-routing.module';
import {ListaDeHeroisComponent} from "./lista-de-herois/lista-de-herois.component";
import { DetalhesHeroiComponent } from './detalhes-heroi/detalhes-heroi.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    ListaDeHeroisComponent,
    DetalhesHeroiComponent,
    LoginComponent,
  ],
  imports: [
    CommonModule,
    HeroisRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class HeroisModule { }
