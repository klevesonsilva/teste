import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "./Service/auth.guard";

const routes: Routes = [
  {path: '', loadChildren:() => import('src/app/herois/herois.module').then(m => m.HeroisModule)}
  //canActivate:[AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
