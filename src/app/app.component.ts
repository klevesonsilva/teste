import {Component} from '@angular/core';
import {HeroisPage} from "./herois/lista-de-herois/herois";
import {AuthService} from "./Service/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SuperHerois';

  exibirMenu: boolean = false;

  constructor(private authService: AuthService) {

  }

  ngOnInit() {
    this.exibirMenu = JSON.parse(this.authService.usuarioEstaAutenticado().toLowerCase());
  }


}
